package Interfaces;


import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.GET;

/**
 * Created by a on 7/18/2016.
 */
public interface RestInterface {

    @GET("/57ecb4a00f0000540bbca2e9")
    public void getRecords(

            Callback<Response> callback);

}
