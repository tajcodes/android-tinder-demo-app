package Models;

/**
 * Created by root on 9/9/16.
 */
public class Users {

    private String name, work;
    private int age, id, imageUrl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(int imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Users(int id, String name, int imageUrl, String work, int age) {
        this.id = id;
        this.name = name;
        this.imageUrl = imageUrl;
        this.work = work;
        this.age = age;
    }

    public String toString() {
        return (this.getId() + ";" + this.getName() +
                ";"+this.getImageUrl() + ";" + this.getWork() +";"+ this.getAge()
        );
    }

    public String NameToString() {
        return (this.getName());
    }


}
