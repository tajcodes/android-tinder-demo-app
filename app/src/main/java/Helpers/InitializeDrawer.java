package Helpers;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.root.tinderdemo.ChatRoom;
import com.example.root.tinderdemo.Homepage;
import com.example.root.tinderdemo.Profile;
import com.example.root.tinderdemo.people;

import java.util.ArrayList;

/**
 * Created by a on 10/25/2016.
 */

public class InitializeDrawer {
    public void setDrawerItems(final Context ctx, ArrayList<String> listDrawerItems, ListView drawer_list)
    {
        listDrawerItems.add("People");
        listDrawerItems.add("Chat");
        listDrawerItems.add("Profile");
        ArrayAdapter adapter=new ArrayAdapter(ctx,android.R.layout.simple_list_item_1,listDrawerItems);


        drawer_list.setAdapter(adapter);
        drawer_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position==0)
                {
                    ctx.startActivity(new Intent(ctx,people.class));
                }
                else if (position==1)
                {
                    ctx.startActivity(new Intent(ctx,ChatRoom.class));
                }
                else if(position==2)
                {
                    ctx.startActivity(new Intent(ctx,Profile.class));
                }
            }
        });
    }
}
