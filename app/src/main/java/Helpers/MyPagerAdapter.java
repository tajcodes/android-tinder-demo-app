package Helpers;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by root on 12/9/16.
 */
public class MyPagerAdapter extends FragmentPagerAdapter {

    private int NUM_ITEMS = 3;

    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        /*
         * IMPORTANT: This is the point. We create a RootFragment
         * acting as a container for other fragments
         */
       /* if (position == 0)
            return new people();
        if (position == 1)
            return new ChatRoom();
        else
            return new Profile();*/
        return null;
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

}