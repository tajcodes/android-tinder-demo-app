package Helpers;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.root.tinderdemo.R;

import java.util.ArrayList;

import Models.Users;

/**
 * Created by root on 13/9/16.
 */
public class FindPeopleSwipeAdapter extends ArrayAdapter<Users>{

    public FindPeopleSwipeAdapter(Context context, ArrayList<Users> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Users user = getItem(position);
        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item, parent, false);
        }
        TextView tvName = (TextView) convertView.findViewById(R.id.name);
        TextView tvAge = (TextView) convertView.findViewById(R.id.age);
        ImageView tvImageView = (ImageView) convertView.findViewById(R.id.image);
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"fonts/moon.otf");
        tvAge.setTypeface(tf);
        tvName.setTypeface(tf);
        tvName.setText(user.getName());
        tvAge.setText(user.getAge()+"");
        tvImageView.setImageResource(user.getImageUrl());
        return convertView;
    }


}
