package Helpers;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.root.tinderdemo.R;

import java.util.ArrayList;

import Models.Users;

/**
 * Created by root on 12/9/16.
 */
public class UserProfileAdapter extends ArrayAdapter<Users> {
    Context context;
    public String username = "";
    public UserProfileAdapter(Context context, ArrayList<Users> users) {
        super(context, 0, users);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Users user = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.user_profile_detail_item, parent, false);
        }
        // Lookup view for data population
        TextView tvName = (TextView) convertView.findViewById(R.id.name);
        TextView tvHome = (TextView) convertView.findViewById(R.id.age);
        TextView tvWork = (TextView) convertView.findViewById(R.id.work);
        final Button button = (Button) convertView.findViewById(R.id.connect);
        username = user.getName();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context.getApplicationContext(), username + " has been sent request.", Toast.LENGTH_SHORT).show();
                button.setText("Waiting to Connect");
            }
        });
        TextView tvshortDescription = (TextView) convertView.findViewById(R.id.shortDescription);
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"fonts/moon.otf");
        tvName.setTypeface(tf);
        tvHome.setTypeface(tf);
        tvWork.setTypeface(tf);
        tvshortDescription.setTypeface(tf);
        // Populate the data into the template view using the data object
        tvName.setText(user.getName());
        tvHome.setText(", "+user.getAge());
        tvWork.setText(user.getWork());
        tvshortDescription.setText("Lorem ipsum dolor sit amet, fastidii periculis sea ut, sed blandit sententiae ut.");
        // Return the completed view to render on screen
        return convertView;
    }

}