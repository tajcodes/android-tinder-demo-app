package com.example.root.tinderdemo;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import Helpers.InitializeDrawer;

public class ChatRoom extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;

    private ArrayList<String> listDrawerItems=new ArrayList<>();
    private ListView drawer_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_chat_room);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        this.setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.nevigation);
        toolbar.setTitle("Chat Room");
        TextView t = (TextView) findViewById(R.id.chatRoom);
        Typeface tf = Typeface.createFromAsset(ChatRoom.this.getAssets(),"fonts/moon.otf");
        t.setTypeface(tf);
        initUi();

    }
    private void initUi() {
       /* mViewPager = (ViewPager) findViewById(R.id.vp);
        mViewPager.setOffscreenPageLimit(3);
        navigationTabStrip = (NavigationTabStrip)findViewById(R.id.nts);*/
        mDrawerLayout= (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer_list= (ListView) findViewById(R.id.list_drawer);
        InitializeDrawer drawerObj=new InitializeDrawer();
        drawerObj.setDrawerItems(ChatRoom.this,listDrawerItems,drawer_list);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        mDrawerLayout.openDrawer(Gravity.START);
        return super.onOptionsItemSelected(menuItem);
    }

}
