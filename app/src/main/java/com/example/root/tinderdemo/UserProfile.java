package com.example.root.tinderdemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

import Helpers.ParallaxScollListView;
import Helpers.UserProfileAdapter;
import Models.Users;

public class UserProfile extends AppCompatActivity {


    private ParallaxScollListView mListView;
    private ImageView mImageView;
    private String userData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        Intent intent = getIntent();
        userData = intent.getStringExtra("userDetails");
        mListView = (ParallaxScollListView) findViewById(R.id.layout_listview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        View header = LayoutInflater.from(this).inflate(R.layout.listview_header, null);
        mImageView = (ImageView) header.findViewById(R.id.layout_header_image);
        String[] userDetail = userData.split(";");
        for(int i = 0; i < userDetail.length ; i++) {
            Log.d("userDetail", userDetail[i]);
        }

        Users user = new Users(Integer.parseInt(userDetail[0]), userDetail[1], Integer.parseInt(userDetail[2]),
                    userDetail[3], Integer.parseInt(userDetail[4]));
        getSupportActionBar().setTitle(userDetail[0]);
        mImageView.setImageResource(Integer.parseInt(userDetail[2]));
        ArrayList<Users> arrayOfUsers = new ArrayList<Users>();
        arrayOfUsers.add(user);
        UserProfileAdapter adapter = new UserProfileAdapter(this, arrayOfUsers);

        mListView.setZoomRatio(ParallaxScollListView.ZOOM_X2);
        mListView.setParallaxImageView(mImageView);
        mListView.addHeaderView(header);

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_expandable_list_item_1,
                new String[]{
                        "First Name"
                }
        );
        mListView.setAdapter(adapter);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

}
