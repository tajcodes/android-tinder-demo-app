package com.example.root.tinderdemo;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.gigamole.navigationtabstrip.NavigationTabStrip;

import java.util.ArrayList;

import Helpers.InitializeDrawer;

public class Homepage extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    ActionBarDrawerToggle mDrawerToggle;

    private ArrayList<String> listDrawerItems=new ArrayList<>();
    private ListView drawer_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);
       /* getSupportActionBar().setDisplayHomeAsUpEnabled(true); // also required
        if (Build.VERSION.SDK_INT >= 18) {
            getSupportActionBar().setHomeAsUpIndicator(
                    getResources().getDrawable(R.drawable.nevigation));
        }*/
         initUi();
        setUI();
    }

    private void initUi() {
       /* mViewPager = (ViewPager) findViewById(R.id.vp);
        mViewPager.setOffscreenPageLimit(3);
        navigationTabStrip = (NavigationTabStrip)findViewById(R.id.nts);*/
        mDrawerLayout= (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer_list= (ListView) findViewById(R.id.list_drawer);
    }

    private void setUI() {
       /* mViewPager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
        mViewPager.setCurrentItem(0);*/

       // navigationTabStrip.setTitles("People", "Chat", "Profile");
        InitializeDrawer drawerObj=new InitializeDrawer();
        drawerObj.setDrawerItems(Homepage.this,listDrawerItems,drawer_list);


//        navigationTabStrip.setTitleSize(34);
       /* navigationTabStrip.setStripColor(Color.DKGRAY);
        navigationTabStrip.setStripWeight(8);
        navigationTabStrip.setStripFactor(2);
        navigationTabStrip.setStripType(NavigationTabStrip.StripType.LINE);
        navigationTabStrip.setStripGravity(NavigationTabStrip.StripGravity.BOTTOM);
        navigationTabStrip.setTypeface("fonts/moon.otf");
        navigationTabStrip.setCornersRadius(3.5f);
        navigationTabStrip.setAnimationDuration(200);
        navigationTabStrip.setInactiveColor(Color.GRAY);
        navigationTabStrip.setActiveColor(Color.DKGRAY);
        navigationTabStrip.setViewPager(mViewPager, 0);
        navigationTabStrip.setTabIndex(0);*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("On Activity Result", requestCode+ "");
    }
}

