package com.example.root.tinderdemo;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.linkedin.platform.APIHelper;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;

import org.json.JSONException;
import org.json.JSONObject;

import Helpers.SharedPreferenceHelper;

public class SetUpProfile1 extends AppCompatActivity {

    public String USER_LOGGED_IN_THROUGH = "user_logged_in_through";

//    LinkedIn
    private static final String host = "api.linkedin.com";
    private static final String topCardUrl = "https://" + host + "/v1/people/~:" +
                "(email-address,formatted-name,date-of-birth,educations,courses,skills,phone-numbers,public-profile-url,industry,headline,location,specialties,positions,picture-url,picture-urls::(original))";

    //    ImageView imageView;
    TextView txtuserName, txtemail, txtDOB, txtgender, txtsave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_up_profile1);

        initUI();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Set Up Profile");

        if(SharedPreferenceHelper.getSharedPreferenceInt(this, USER_LOGGED_IN_THROUGH, 0) == 0){
            Log.d("Facebook", "Got into Facebook");
            fetchLinkedInDetails();
        } else {
            Log.d("", "Got into LinkedIn");
            fetchFacebookDetails();
        }

        txtsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SetUpProfile1.this, people.class);
                startActivity(intent);
                finish();
            }
        });

    }

    private void initUI() {
//        imageView = (ImageView) findViewById(R.id.userImage);
        txtuserName = (TextView) findViewById(R.id.usr_name);
        txtsave = (TextView) findViewById(R.id.txt_save);
    }

    private void fetchLinkedInDetails() {
        linkededinApiHelper();
    }

    private void linkededinApiHelper() {
        APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
        apiHelper.getRequest(SetUpProfile1.this, topCardUrl, new ApiListener() {
            @Override
            public void onApiSuccess(ApiResponse result) {
                try {
                    Log.d("LinkedIn API Success", result.getResponseDataAsJson().toString());
                    setprofile(result.getResponseDataAsJson());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onApiError(LIApiError error) {
                // ((TextView) findViewById(R.id.error)).setText(error.toString());
                Log.d("LinkedIn", error.toString());
            }
        });
    }

    private void setprofile(JSONObject response) {
        try {
            txtuserName.setText(response.get("formattedName").toString());
//            imageView.setImageURI(Uri.parse(response.getString("pictureUrl")));
//            Glide.with(this).load(response.getString("pictureUrl")).into(imageView);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void fetchFacebookDetails() {

    }
}
