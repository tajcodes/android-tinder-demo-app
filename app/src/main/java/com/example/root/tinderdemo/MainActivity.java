package com.example.root.tinderdemo;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.linkedin.platform.AccessToken;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import Helpers.SharedPreferenceHelper;

public class MainActivity extends AppCompatActivity {

    private LoginButton loginButton;
    private boolean throughLinkedIn;
    private Button btnLinkedIn;
    private String TAG = "LinkZone";

//    Shared Preference
    public String USER_LOGGED_IN = "user_logged_in";

//    0: Facebook, 1: LinkedIn
    public String USER_LOGGED_IN_THROUGH = "user_logged_in_through";

//    LinkedIn
    public static final String PACKAGE = "com.example.root.tinderdemo";

    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        setContentView(R.layout.activity_main);
        btnLinkedIn = (Button)findViewById(R.id.linkedIn_login);
        boolean user_logged_in = SharedPreferenceHelper.getSharedPreferenceBoolean(MainActivity.this, USER_LOGGED_IN, false);
        Log.d("Tinder: Logged In", user_logged_in + " ");
        if(user_logged_in) {
            Intent intent = new Intent(MainActivity.this, people.class);
            startActivity(intent);
            finish();
        }

       /* TextView label = (TextView)findViewById(R.id.label);
        Typeface tf = Typeface.createFromAsset(getAssets(),"fonts/moon.otf");
        label.setTypeface(tf);*/

//        Facebook
        callbackManager = CallbackManager.Factory.create();

        loginButton = (LoginButton)findViewById(R.id.login_button);

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                throughLinkedIn = false;
                SharedPreferenceHelper.setSharedPreferenceBoolean(MainActivity.this, USER_LOGGED_IN, true);
                SharedPreferenceHelper.setSharedPreferenceInt(MainActivity.this, USER_LOGGED_IN_THROUGH, 0);
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.d("Facebook", response.toString());
                            }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, name, work");
                request.setParameters(parameters);
                request.executeAsync();
//                Intent intent = new Intent(MainActivity.this, Homepage.class);
//                startActivity(intent);
//                finish();
                Log.d("Tinder : Facebook ",
                        "User ID: "
                                + loginResult.getAccessToken().getUserId()
                                + "\n" +
                                "Auth Token: "
                                + loginResult.getAccessToken().getToken()
                );
            }

            @Override
            public void onCancel() {
                Log.d("Tinder : Facebook", "Cancel");
            }

            @Override
            public void onError(FacebookException e) {
                Log.d("Tinder : Facebook", "Error"  + e.toString());
                Toast.makeText(getApplicationContext(), "Error! Please make sure you are connected To Internet.", Toast.LENGTH_LONG).show();
            }
        });

        btnLinkedIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                throughLinkedIn = true;
                LISessionManager.getInstance(getApplicationContext()).init(MainActivity.this, buildScope(), new AuthListener() {
                    @Override
                    public void onAuthSuccess() {
                        // Authentication was successful.  You can now do
                        // other calls with the SDK.
                        Log.d("Hello", "OnAuthSuccess: "+ LISessionManager.getInstance(getApplicationContext()).getSession().getAccessToken().toString());


                    }

                    @Override
                    public void onAuthError(LIAuthError error) {
                        // Handle authentication errors
                        Log.d("Hello", "OnAuthError: "+ error.toString() );
                    }
                }, true);
            }

        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(throughLinkedIn) {
            LISessionManager.getInstance(getApplicationContext()).onActivityResult(this, requestCode, resultCode, data);
            Log.d(TAG, throughLinkedIn + "LinkedIn"+ data.toString());
            SharedPreferenceHelper.setSharedPreferenceBoolean(MainActivity.this, USER_LOGGED_IN, true);
            Intent intent = new Intent(MainActivity.this, SetUpProfile1.class);
            intent.putExtra(USER_LOGGED_IN_THROUGH, 1);
            startActivity(intent);
            finish();
        }
        else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
            Log.d(TAG, throughLinkedIn + "FaceBook");
        }
    }

    private static Scope buildScope() {
        return Scope.build(Scope.R_EMAILADDRESS, Scope.R_BASICPROFILE);
    }

}
