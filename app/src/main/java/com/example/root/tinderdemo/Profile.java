package com.example.root.tinderdemo;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;

import java.util.ArrayList;

import Helpers.InitializeDrawer;
import Helpers.SharedPreferenceHelper;

public class Profile extends AppCompatActivity {
    public String USER_LOGGED_IN = "user_logged_in";
    public String USER_LOGGED_IN_THROUGH = "user_logged_in_through";
    private DrawerLayout mDrawerLayout;

    private ArrayList<String> listDrawerItems=new ArrayList<>();
    private ListView drawer_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_profile);
        FacebookSdk.sdkInitialize(Profile.this);

        SharedPreferenceHelper.getSharedPreferenceInt(Profile.this, USER_LOGGED_IN_THROUGH, 0);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        this.setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.nevigation);
        toolbar.setTitle("Profile");

        TextView t = (TextView) findViewById(R.id.logout);
        TextView pv = (TextView) findViewById(R.id.profileView);
        Typeface tf = Typeface.createFromAsset(Profile.this.getAssets(),"fonts/moon.otf");
        t.setTypeface(tf);
        pv.setTypeface(tf);
        initUi();
        t.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlert();
            }
        });
    }


    private void initUi() {
       /* mViewPager = (ViewPager) findViewById(R.id.vp);
        mViewPager.setOffscreenPageLimit(3);
        navigationTabStrip = (NavigationTabStrip)findViewById(R.id.nts);*/
        mDrawerLayout= (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer_list= (ListView) findViewById(R.id.list_drawer);
        InitializeDrawer drawerObj=new InitializeDrawer();
        drawerObj.setDrawerItems(Profile.this,listDrawerItems,drawer_list);
    }

    public void showAlert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(Profile.this);

        builder.setTitle("Logout ?");
        builder.setMessage("Are you sure you want to logout");
        builder.setPositiveButton("Logout", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferenceHelper.setSharedPreferenceBoolean(Profile.this, USER_LOGGED_IN, false);
                LoginManager.getInstance().logOut();
                Intent intent = new Intent(Profile.this, MainActivity.class);
                startActivity(intent);
                Profile.this.finish();
            }
        });
        builder.create();

        builder.show();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        mDrawerLayout.openDrawer(Gravity.START);
        return super.onOptionsItemSelected(menuItem);
    }
}
