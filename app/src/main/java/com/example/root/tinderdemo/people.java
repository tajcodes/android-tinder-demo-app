package com.example.root.tinderdemo;


import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

import Adapters.PeopleListAdapter;
import Helpers.InitializeDrawer;
import Interfaces.RestInterface;
import Models.Users;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class people extends AppCompatActivity implements View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    private DrawerLayout mDrawerLayout;

    private ArrayList<String> listDrawerItems = new ArrayList<>();
    private ListView drawer_list;
    private ArrayList<Users> al;
    private GoogleApiClient googleApiClient;
    private Context context;
    private AVLoadingIndicatorView avLoadingIndicatorView;
    private RecyclerView recyclerView;
    final int[] imagesArr = new int[]{R.drawable.tinder1, R.drawable.tinder2, R.drawable.tinder3, R.drawable.tinder4};
    private double currentLatitude, currentLongitude;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 199;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  setRetainInstance(true);
//       Get Location First


        setContentView(R.layout.fragment_people);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        this.setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.nevigation);
        toolbar.setTitle("Near By");

       // toolbar.setLogo(R.drawable.ic_launcher);
        ifLocationIsEnabled();
        avLoadingIndicatorView = new AVLoadingIndicatorView(people.this);

        recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(people.this, 2);
        recyclerView.setLayoutManager(layoutManager);
        initUi();
        getNearByPeople();
    }


    private void initUi() {
       /* mViewPager = (ViewPager) findViewById(R.id.vp);
        mViewPager.setOffscreenPageLimit(3);
        navigationTabStrip = (NavigationTabStrip)findViewById(R.id.nts);*/
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer_list = (ListView) findViewById(R.id.list_drawer);
        InitializeDrawer drawerObj = new InitializeDrawer();
        drawerObj.setDrawerItems(people.this, listDrawerItems, drawer_list);
    }

    private void ifLocationIsEnabled() {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(people.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            //**************************
            builder.setAlwaysShow(true); //this is the key ingredient
            //**************************

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());

            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result.getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can initialize location
                            // requests here.

                            if (ActivityCompat.checkSelfPermission(people.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(people.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                            if (location == null) {
                                Log.d("GoogleAPIClient", "Success but Null");
                                // Blank for a moment...
                            } else {
                                Log.d("GoogleAPIClient", "Success and " + location.toString());
                                handleNewLocation(location);
                            }
                            ;
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(
                                        people.this, 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getLocation();
        Log.d("GoogleAPIClient", "Connected");
    }

    private void handleNewLocation(Location location) {
        Log.d("Google Location", location.toString());
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
       // getNearByPeople();
    }

    private void getNearByPeople() {
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint("http://www.mocky.io/v2/")
                .build();

        RestInterface api = adapter.create(RestInterface.class);

        api.getRecords(
                new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        Log.d("Response", response.toString());

                        try {
                            al = new ArrayList<Users>();
                            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getBody().in()));
                            String output = reader.readLine();
                            final int[] imagesArr = new int[]{R.drawable.tinder1, R.drawable.tinder2, R.drawable.tinder3, R.drawable.tinder4};
                            JSONTokener jsonTokener = new JSONTokener(output);
                            JSONArray array = new JSONArray(jsonTokener);

                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);
                                String name = object.getString("first_name") + " " + object.getString("last_name");
                                al.add(new Users(3, name, imagesArr[new Random().nextInt(imagesArr.length)], "Schindlr Pvt. Lmt", 24));
                            }
                            avLoadingIndicatorView.hide();
                            PeopleListAdapter adapter = new PeopleListAdapter(people.this, al);
                            recyclerView.setAdapter(adapter)
                            ;

                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.d("ResponseErrorPeople", error.toString());
                    }
                }
        );

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d("GoogleAPIClient", "Connection Suspended" + i);
    }

    public void getLocation() {
        Log.d("getLocation", "in Get Location");
        if (ActivityCompat.checkSelfPermission(people.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(people.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location == null) {
            Log.d("GetLocation", "Lost It");
            // Blank for a moment...
        }
        else {
            Log.d("GetLocation", "Connected+ location: " + location.toString());
            handleNewLocation(location);
        };
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(people.this, ConnectionResult.RESOLUTION_REQUIRED);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.d("GoogleAPIClient", "Connection Failed");
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("OnActivity Result", "Result People"+ requestCode);
        switch (requestCode) {
// Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:

                        getLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        //keep asking if imp or do whatever
                        break;
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
            mDrawerLayout.openDrawer(Gravity.START);
        return super.onOptionsItemSelected(menuItem);
    }
}
