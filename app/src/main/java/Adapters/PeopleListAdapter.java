package Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.root.tinderdemo.R;
import com.example.root.tinderdemo.UserProfile;

import java.util.ArrayList;
import Models.Users;

/**
 * Created by root on 14/9/16.
 */
public class PeopleListAdapter extends RecyclerView.Adapter<PeopleListAdapter.ViewHolder> {

    private ArrayList<Users> user;
    private Context context;

    public PeopleListAdapter(Context context, ArrayList<Users> user) {
        this.context = context;
        this.user = user;
    }

    public PeopleListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i){
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PeopleListAdapter.ViewHolder viewHolder, int i ) {
        Log.d("Tinder" , user.get(i).getName().toString());
        final int a  = i;
        viewHolder.tvName.setText(user.get(i).getName());
        viewHolder.tvDistance.setText("3 miles away");
        viewHolder.tvWork.setText(user.get(i).getWork());
        viewHolder.tvImageView.setImageResource(user.get(i).getImageUrl());
        viewHolder.tvCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("TINDER", user.get(a).toString());
                Intent intent = new Intent(v.getContext(), UserProfile.class);
                intent.putExtra("userDetails", user.get(a).toString());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeSceneTransitionAnimation((Activity)v.getContext(), viewHolder.tvImageView, v.getContext().getString(R.string.user_image_trans));
                    v.getContext().startActivity(intent, options.toBundle());
                }
                else {
                    v.getContext().startActivity(intent);
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return user.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvWork, tvDistance;
        ImageView tvImageView;
        CardView tvCard;
        Context context;
        public ViewHolder(View view) {
            super(view);
            context = view.getContext();
            tvCard = (CardView) view.findViewById(R.id.card_view);
            tvName = (TextView) view.findViewById(R.id.name);
            tvWork = (TextView) view.findViewById(R.id.work);
            tvDistance = (TextView) view.findViewById(R.id.distance);
            tvImageView = (ImageView) view.findViewById(R.id.userImage);
            tvWork.setTypeface(Typeface.createFromAsset(view.getContext().getAssets(),"fonts/Roboto-Light.ttf"));
            tvName.setTypeface(Typeface.createFromAsset(view.getContext().getAssets(),"fonts/Roboto-Regular.ttf"));
            tvDistance.setTypeface(Typeface.createFromAsset(view.getContext().getAssets(),"fonts/Roboto-LightItalic.ttf"));
        }
    }
}

